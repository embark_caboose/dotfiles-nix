{
  description = "siempie's special";

  # inputs
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  # outputs
  outputs = { self, nixpkgs, nixpkgs-unstable,home-manager, ...} @ inputs: let
    inherit (self) outputs;
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
    };
    unstableOverlay = final: prev: {
      unstable = import nixpkgs-unstable {
        system = "x86_64-linux";
	      config.allowUnfree = true;
      };
    };

    lib = nixpkgs.lib; in {
    nixosConfigurations = {

      # laptop - itclt75
      itclt75 = lib.nixosSystem {
        specialArgs = { inherit inputs outputs; };
        specialArgs.channels = { inherit nixpkgs nixpkgs-unstable; };
        inherit system;
        modules = [
          ({ 
             nixpkgs = {
               overlays = [ unstableOverlay ];
               config.allowUnfree = true;
             };
           })
          ./nix/systems/itclt75/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.simon = {
              imports = [
                ./nix/home
              ];
            };
          }
        ];
      };

      # laptop - talathiel
      talathiel = lib.nixosSystem {
        specialArgs = { inherit inputs outputs; };
        specialArgs.channels = { inherit nixpkgs nixpkgs-unstable; };
        inherit system;
        modules = [
          ({ 
             nixpkgs = {
               overlays = [ unstableOverlay ];
               config.allowUnfree = true;
             };
           })
          ./nix/systems/talathiel/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.simon = {
              imports = [
                ./nix/home
              ];
            };
          }
        ];
      };

      # desktop - phaedra
      phaedra = lib.nixosSystem {
        specialArgs = { inherit inputs outputs; };
        specialArgs.channels = { inherit nixpkgs nixpkgs-unstable; };
        inherit system;
        modules = [
          ({ 
             nixpkgs = {
               overlays = [ unstableOverlay ];
               config.allowUnfree = true;
             };
           })
          ./nix/systems/phaedra/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.simon = {
              imports = [
                ./nix/home
              ];
            };
          }
        ];
      };
    };
  };
}
