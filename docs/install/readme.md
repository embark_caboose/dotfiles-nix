base: [NixOS 23.11](https://channels.nixos.org/nixos-23.11/latest-nixos-minimal-x86_64-linux.iso)

This simple guide will install NixOS 23.11 using the following disk layout.
| mountpoint | filesystem | partition | size     |
|------------| ---------- | --------- | -------- |
| /boot      | fat32      | /dev/sda1 | 500MB    |  
| /          | ext4       | /dev/sda2 | the rest |

Make sure to use the correct disk. 😊  

## Installation
```
nix-shell -p git
```

Partitions
```
g (gpt disk label)
n
1 (partition number [1/128])
2048 first sector
+500M last sector (boot sector size)
t
1 (EFI System)
n
2
default (fill up partition)
default (fill up partition)
w (write)
```

Filesystems
```
sudo mkfs.fat -F 32 /dev/sda1
sudo mkfs.ext4 /dev/sda2 -L root
sudo fatlabel /dev/sda1 boot
sudo mount /dev/disk/by-label/root /mnt
sudo mkdir -p /mnt/boot
sudo mount /dev/disk/by-label/boot /mnt/boot
```

Clone this repo
```
git clone https://git.simoncor.net/siempie/dotfiles-nix.git
```

Start Installation
```
cd dotfiles
sudo nixos-install --flake ./dotfiles-nix#<phaedra|talathiel>
```

Reboot
```
sudo reboot
```


## Finalize
Clone dotfiles
```
git clone https://git.simoncor.net/siempie/dotfiles-nix.git ~/.dotfiles
```

Login to all apps
```
bitwarden
firefox + bitwarden plugin
google-chrome + bitwarden -plugin
nextcloud
vscode
spotify
```

Headscale
```
sudo tailscale up --login-server=https://vpn.mirahsimon.us:443 --accept-routes --operator=$USER
```
