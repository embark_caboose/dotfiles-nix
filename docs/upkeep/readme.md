## maintenance
system rebuild
```
sudo nixos-rebuild switch --upgrade --flake ~/.dotfiles#<system_name>
```

firmware updates
```
sudo fwupdmgr update
```
