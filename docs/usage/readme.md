# Usage

### Hyprland
#### Keyboard bindings
    
`mainMod = SUPER`

| applications  |                      | 
| --------------| -------------------- |
| mM + enter    | terminal             |
| mM + C        | clipboard manager    |
| mM + D        | application launcher |
| mM + E        | file manager         |
| mM + L        | lock screen          |
| mM + S        | take area screenshot |
| ALT SHIFT + S | take full screenshot |


| focus      |                  |
| ---------- | ---------------- |
| mM + up    | move focus up    |
| mM + down  | move focus down  |
| mM + left  | move focus left  |
| mM + right | move focus right |


| window control    |                   |
| ----------------- | ----------------- |
| mM + F            | window fullscreen |
| mM + J            | toggle split      |
| mM + P            | toggle pseudo     |
| mM + Q            | window close      |
| mM + V            | toggle float      |
| mM + mouse_right  | move window       |
| mM + mouse_left   | resize window     |
| ALT SHIFT + up    | move window up    |
| ALT SHIFT + down  | move window down  |
| ALT SHIFT + left  | move window left  |
| ALT SHIFT + right | move window right |


| workspace             |                                  |
| --------------------- |  ------------------------------- |
| mM + 0-9              | move focus to workspace 0-9      |
| mM + SHIFT + 0-9      | move window to workspace 0-9     |
| mM + mouse_wheel_up   | move focus to previous workspace |
| mM + mouse_wheel_down | move focus to next workspace     |


#### Autostart (in order)
 - waybar
 - tailscale
 - clipse
 - blueman-tray
 - solaar
 - nextcloud-desktop
 - bitwarden (workspace 1)
 - firefox   (workspace 2)
 - alacritty (workspace 3)

## Theme
TODO
