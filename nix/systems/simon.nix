{ config, pkgs, ... }: {

  # enable zsh
  programs.zsh.enable = true;

  # user simon
  users.users.simon = {
    isNormalUser    = true;
    description     = "Simon Cornet";
    initialPassword = "changeme";
    shell           = pkgs.zsh;
    extraGroups     = [ 
      "libvirtd"
      "networkmanager"
      "wheel"
    ];
  };
}
