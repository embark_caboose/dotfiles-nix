{ config, pkgs, ... }: {

  # imports
  imports = [ 
    ../default.nix
    ./hardware-configuration.nix 
  ];

  # opengl
  hardware.opengl = {
    extraPackages = with pkgs; [
      intel-media-driver
      intel-vaapi-driver
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  # networking
  networking.hostName = "itclt75";
}
