{ config, lib, pkgs, modulesPath, ... }:
{

  # imports
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  # bootloader
  boot.initrd.kernelModules          = [ "kvm-intel" ];
  boot.initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.luks = {
    devices = {

      # encrypted root
      "luks-78011bc3-67ab-49d6-89f5-be5f50a6c013" = {
        device = "/dev/disk/by-uuid/78011bc3-67ab-49d6-89f5-be5f50a6c013";

      };
    };
  };

  # filesystems - boot
  fileSystems."/boot" = { 
    device = "/dev/disk/by-label/BOOT";
    fsType = "vfat";
  };

  # filesystems - root
  fileSystems."/" = { 
    device = "/dev/disk/by-label/root";
    fsType = "ext4";
  };

  # swap
  swapDevices = [ {
    device = "/swapfile";
    size   = 16384;
  } ];

  # networking
  networking.useDHCP = lib.mkDefault true;

  # hardware
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
