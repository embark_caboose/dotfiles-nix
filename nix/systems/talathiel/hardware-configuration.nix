{ config, lib, pkgs, modulesPath, ... }:
{

  # imports
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  # bootloader
  boot.initrd.kernelModules          = [ "kvm-intel" ];
  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];

  # filesystems - boot
  fileSystems."/boot" = { 
    device = "/dev/disk/by-label/boot";
    fsType = "vfat";
  };

  # filesystems - root
  fileSystems."/" = { 
    device = "/dev/disk/by-label/root";
    fsType = "ext4";
  };

  # swap
  swapDevices = [ {
    device = "/swapfile";
    size   = 4096;
  } ];

  # networking
  networking.useDHCP = lib.mkDefault true;

  # hardware
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
