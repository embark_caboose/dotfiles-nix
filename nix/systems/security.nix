{ config, pkgs, ... }: {

  # pam
  security.pam.services.swaylock = {
    text = ''auth include login'';
  };

  # sudo
  security.sudo.wheelNeedsPassword = false; 

  # enable realtimekit
  security.rtkit.enable = true;
}
