{ config, pkgs, ... }: {

  # bootloader
  boot.loader.efi.canTouchEfiVariables        = true;
  boot.loader.systemd-boot.enable             = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.systemd-boot.consoleMode        = "max"; 

  # kernel stuff
  boot.kernelModules  = [ "tcp_bbr" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # network go brr
  boot.kernel.sysctl = {
    "net.ipv4.tcp_congestion_control" = "bbr";
  };

  # make boot pretty
  boot.plymouth.enable       = true;
  boot.kernelParams          = [ "quiet" ];
  boot.initrd.systemd.enable = true;

  # make console pretty
  console = {
    earlySetup = true;
    font       = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";
    keyMap     = "us";
    packages   = with pkgs; [ terminus_font ];
  };
}
