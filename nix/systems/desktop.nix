{ config, pkgs, ... }: {

  # x11
  services.xserver = {
    enable      = true;
    xkb.layout  = "us";
    xkb.variant = "";
  };

  # xdg portals
  xdg.portal.enable       = true;
  xdg.portal.extraPortals = with pkgs; [
    xdg-desktop-portal-wlr
  ];

  # gnome
  services.xserver.displayManager.gdm.enable   = true;
  services.xserver.displayManager.gdm.wayland  = true;
  services.xserver.desktopManager.gnome.enable = true;
  
  # hyprland
  programs.hyprland.enable          = true;
  programs.hyprland.xwayland.enable = true;

  # environment variables for hyprland
  environment.sessionVariables = {

    # hint electron apps to use wayland
    NIXOS_OZONE_WL = "1";

  };
}
