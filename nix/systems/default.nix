{ config, lib, pkgs, ... }: {

  # imports
  imports = [ 
    ./bootloader.nix
    ./desktop.nix
    ./hardware.nix
    ./locality.nix
    ./networking.nix
    ./nix.nix
    ./packages.nix
    ./security.nix
    ./simon.nix
    ./sound.nix
    ./steam.nix
    ./virtualisation.nix
  ];

  # system state version
  system.stateVersion = "23.11";
}
