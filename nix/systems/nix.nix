{ config, lib, pkgs, ... }: {

  # nix
  nix = {

    # garbage collection
    gc.automatic = true;
    gc.dates     = "weekly";
    gc.options   = "--delete-older-than 7d";

    # nix package with flakes
    package = pkgs.nixFlakes;

    # nix settings
    settings = {
      auto-optimise-store = true;
    };

    # extra options
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
  };

  # nix pkgs
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
