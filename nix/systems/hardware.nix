{ config, lib, pkgs, ... }: {

  # hardware
  hardware.opengl.enable                 = true;
  hardware.enableRedistributableFirmware = true;

  # hardware logitech
  hardware.logitech.wireless.enable          = true;
  hardware.logitech.wireless.enableGraphical = true;

  # enable bluetooth
  hardware.bluetooth.enable      = true;
  hardware.bluetooth.powerOnBoot = true;

  # power management
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  powerManagement.powertop.enable = true;
}
