{ config, pkgs, ... }: {

  # packages
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [

    # system
    btop
    fastfetch
    fwupd
    htop
    intel-gpu-tools
    killall
    radeontop
    pciutils
    pavucontrol
    powertop
    solaar
    spice-vdagent
    sshfs

    # hyprland
    adwaita-qt
    brightnessctl
    flameshot
    grim
    grimblast
    hyprlock
    imagemagick
    jq
    libnotify
    lxappearance-gtk2
    satty
    slurp
    socat
    swaybg
    waybar
    wl-clipboard
    wofi
    xfce.xfce4-settings

    # fonts
    fira-code
    fira-code-nerdfont
    font-awesome
    jetbrains-mono
    nerdfonts
    ubuntu_font_family

    # browsers
    firefox
    google-chrome
    microsoft-edge

    # general cli utilities
    bat
    clipse 
    cmatrix
    distrobox
    git
    screen
    podman
    podman-compose
    vim
    wget

    # kube mgmt
    kubernetes-helm
    kubectl
    talosctl
    openlens

    # general gui utilities
    bitwarden  
    gimp
    meld
    nextcloud-client
    trayscale
    vscode

    # multimedia
    mpv
    multiviewer-for-f1
    spotify
    yt-dlp

    # dns utilities
    dnsutils
    whois

    # office
    filezilla
    remmina
    teams-for-linux
    teamviewer
    teleport_14
    webex

  ];

  # services
  services.blueman.enable  = true;
  services.fwupd.enable    = true;
  services.openssh.enable  = true;
  services.printing.enable = true;
  services.tailscale = {
    enable  = true;
    package = pkgs.unstable.tailscale;
  };
}
