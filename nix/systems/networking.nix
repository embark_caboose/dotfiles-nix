{ config, pkgs, ... }: {

  # networking
  networking.firewall.enable       = false;
  networking.networkmanager.enable = true;

}
