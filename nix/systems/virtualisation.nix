{ config, pkgs, ... }: {

  # setup virt-manager
  programs.virt-manager.enable = true;

  # virtualistation
  virtualisation = {

    # libvirtd
    libvirtd = {
      enable = true;
    };

    # podman
    podman = {
      enable                              = true;
      dockerCompat                        = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };
}
