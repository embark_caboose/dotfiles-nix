{ config, pkgs, ... }: {

  # enable sound
  sound.enable = true;

  # disable pulseaudio
  hardware.pulseaudio.enable = false;

  # enable pipewire
  services.pipewire.enable            = true;
  services.pipewire.pulse.enable      = true;
  services.pipewire.alsa.enable       = true;
  services.pipewire.alsa.support32Bit = true;

}
