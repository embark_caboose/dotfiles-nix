{ config, lib, pkgs, modulesPath, ... }:
{

  # imports
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  # bootloader
  boot.kernelModules                 = [ "kvm-amd" "amdgpu" ];
  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];

  # filesystems - boot
  fileSystems."/boot" = { 
    device = "/dev/disk/by-label/boot";
    fsType = "vfat";
  };

  # filesystems - root
  fileSystems."/" = { 
    device = "/dev/disk/by-label/root";
    fsType = "ext4";
  };

  # swap
  swapDevices = [ {
    device = "/swapfile";
    size   = 4096;
  } ];

  # networking
  networking.useDHCP = lib.mkDefault true;

  # hardware
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # disable nvidia!
  systemd.services.removeNvidia = {
    unitConfig.ConditionPathExists = "/sys/bus/pci/devices/0000:01:00.0/remove";
    wantedBy = [ "multi-user.target" ];
    description = "Remove NVIDIA upon boot";
    serviceConfig = {
      Type = "oneshot";
      User = "root";
      RemainAfterExit = true;
      ExecStart = ''${pkgs.bash}/bin/bash -c "echo 1 > /sys/bus/pci/devices/0000:01:00.0/remove"''; 
    };
  };
}
