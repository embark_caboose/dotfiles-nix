{ config, pkgs, ... }:
{

  # imports
  imports = [ 
    ./alacritty
    ./clipse
    ./flameshot
    ./git
    ./hyprland
    ./mako
    ./mpv
    ./neovim
    ./ssh
    ./theme
    ./tmux
    ./virt-manager
    ./xdg
    ./zsh
  ];

  # user
  home.username      = "simon";
  home.homeDirectory = "/home/simon";

  # fonts
  fonts.fontconfig.enable = true;

  # home-manager
  home.stateVersion = "24.05";
  programs.home-manager.enable = true;
}
