{ config, ... }:
{
  
  # hyprland general config
  wayland.windowManager.hyprland.settings = {

    # windowrulev1
    windowrule = [

      # window rules
      "float, confirm|confirmreset"
      "float, dialog|download"
      "float, error|eog"
      "float, file-roller|file_progress"
      "float, Lxappearance|notification"
      "float, splash"
      "float, title:branchdialog"
      "float, title:Nextcloud"

    ];

    # windowrulev2
    windowrulev2 = [

      # alacritty
      "float,         class:(scratchbox)"
      "size 1200 800, class:(scratchbox)"
      "move center,   class:(scratchbox)"

      # blueman
      "float,         class:(.blueman-manager-wrapped), title:(.blueman-manager-wrapped)"
      "size 1000 800, class:(.blueman-manager-wrapped), title:(.blueman-manager-wrapped)"
      "move center,   class:(.blueman-manager-wrapped), title:(.blueman-manager-wrapped)"

      # clipse
      "float,         class:(clipse)"
      "size 1000 800, class:(clipse)"
      "move center,   class:(clipse)"

      # flameshot
      "float,         class:^(flameshot)$"
      "noanim,        class:^(flameshot)$"

      # save as
      "float,         title:(Save As)"
      "size 1000 800, title:(Save As)"
      "move center,   title:(Save As)"

      # open file
      "float,         title:(Open File)"
      "size 1000 800, title:(Open File)"
      "move center,   title:(Open File)"

      # add folder to workspace
      "float,         title:(Add Folder to Workspace)"
      "size 1000 800, title:(Add Folder to Workspace)"
      "move center,   title:(Add Folder to Workspace)"

      # network manager tui
      "float,         class:(nmtui), title:(Alacritty)"
      "size 1180 792, class:(nmtui), title:(Alacritty)"
      "move center,   class:(nmtui), title:(Alacritty)"

      # nos teletekst
      "float,         class:(nostt), title:(Alacritty)"
      "size 455 660,  class:(nostt), title:(Alacritty)"
      "move center,   class:(nostt), title:(Alacritty)"

      # satty
      "float,         class:(com.gabm.satty), title:(satty)"
      "size 1400 900, class:(com.gabm.satty), title:(satty)"
      "move center,   class:(com.gabm.satty), title:(satty)"

      # solaar
      "float,         class:(solaar), title:(solaar)"
      "size 1000 800, class:(solaar), title:(solaar)"
      "move center,   class:(solaar), title:(solaar)"

      # trayscale
      "float,         class:(dev.deedles.Trayscale), title:(Trayscale)"
      "size 1000 800, class:(dev.deedles.Trayscale), title:(Trayscale)"
      "move center,   class:(dev.deedles.Trayscale), title:(Trayscale)"

      # volume control
      "float,         title:(Volume Control)"
      "size 1000 800, title:(Volume Control)"
      "move center,   title:(Volume Control)"

      # wofi
      "forceinput,  class:(wofi)"
      "windowdance, class:(wofi)"
      "stayfocused, class:(wofi)"

    ];
  };
}
