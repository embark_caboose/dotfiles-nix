{ config, ... }:
{
  
  # hyprland environment variables
  wayland.windowManager.hyprland.settings = {
    
    # environment variables
    env = [

      # firefox
      "MOZ_ENABLE_WAYLAND,1"
      
      # electron
      "ELECTRON_OZONE_PLATFORM_HINT,auto"

      # desktop frameworks
      "CLUTTER_BACKEND,wayland"
      "GDK_BACKEND,wayland,x11"
      "QT_AUTO_SCREEN_SCALE_FACTOR,1"
      "QT_QPA_PLATFORM,wayland;xcb"
      "QT_QPA_PLATFORMTHEME,qt5ct"
      "QT_QPA_PLATFORMTHEME,qt6ct"  
      "QT_SCALE_FACTOR,1"
      "QT_WAYLAND_DISABLE_WINDOWDECORATION,1"
      "XDG_CURRENT_DESKTOP,Hyprland"
      "XDG_SESSION_DESKTOP,Hyprland"
      "XDG_SESSION_TYPE,wayland"

    ];
  };
}
