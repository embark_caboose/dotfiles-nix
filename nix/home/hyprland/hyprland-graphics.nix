{ config, ... }:
{
  
  # hyprland general config
  wayland.windowManager.hyprland.settings = {
    
    # configure monitors
    monitor = [

      # main monitor
      "eDP-1,    1920x1200@60,  760x1440, 1"

      # secundary monitor
      "HDMI-A-1, 1920x1080@60,       0x0, 1"

      # any other monitor
      ",            preferred,       0x0, 1"

    ];

    # unscale xwayland
    xwayland = {
      force_zero_scaling = true;
    };

    # animation section
    animations = {
      enabled = true;

      bezier = [
        "fade,      0.05, 1,   0.1,   1"
        "smoothOut, 0.36, 0,   0.66,  1"
        "smoothIn,  0.25, 1,   0.5,   1"
        "liner,        1, 1,   1,     1"
      ];

      animation =  [
        "windows,      1, 3,   smoothIn"
        "windowsOut,   1, 3,   smoothOut"
        "windowsMove,  1, 3,   default"
        "border,       1, 10,  default"
        "borderangle,  1, 45,  liner, loop"
        "fade,         1, 5,   smoothIn"
        "fadeDim,      1, 5,   smoothIn"
        "workspaces,   1, 8,   fade"
      ];
    };

    # decoration section
    decoration = {

      # blur
      blur = {
        enabled = true;
        size    = "2";
        passes  = "2";
      };

      # windows
      rounding = "16";

      # shadows
      drop_shadow          = true;
      shadow_offset        = "2 2";
      shadow_range         = "8";
      shadow_render_power  = "2";
      shadow_ignore_window = true;
      "col.shadow"         = "0x66000000";

      # opacity
      active_opacity   = "1.0";
      inactive_opacity = "1.0";
    };
  };
}
