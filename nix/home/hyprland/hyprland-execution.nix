{ config, ... }:
{
  
  # hyprland environment variables
  wayland.windowManager.hyprland.settings = {

    # execute always
    exec = [

      # start waybar
      "~/.dotfiles/apps/waybar/startup.sh"

      # set monitor workspaces
      "~/.dotfiles/apps/hyprland/monitor.sh"

      # start blueman
      "blueman-tray"

      # set wallpaper
      "~/.dotfiles/apps/wallpaper/startup.sh"

      # center apps
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"blueman-manager\" --title \"Bluetooth Devices\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"clipse\" --title \"Alacritty\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"com.gabm.satty\" --title \"satty\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"dev.deedles.Trayscale\" --title \"Trayscale\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"nmtui\" --title \"Alacritty\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"nostt\" --title \"Alacritty\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"pavucontrol\" --title \"Volume Control\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"Rofi\"  --title \"rofi -   Apps\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"scratchbox\" --title \"Alacritty\""
      "~/.dotfiles/apps/hyprland/centre_app.sh --class \"solaar\" --title \"solaar\""

    ];

    # execute once
    exec-once = [

      # start tailscale
      "tailscale up"

      # clear and start clipse
      "clipse -clear"
      "clipse -listen"

      # set volume
      "pw-volume change 80%"

      # solaar
      "~/.dotfiles/apps/hyprland/startup/solaar.sh"

      # nextcloud desktop
      "~/.dotfiles/apps/hyprland/startup/nextcloud_desktop.sh"

      # flameshot
      "XDG_CURRENT_DESKTOP=sway flameshot"

      # start default apps
      "[workspace 1 silent] bitwarden"
      "[workspace 2 silent] firefox"
      "[workspace 3 silent] alacritty"

    ];
  };
}
