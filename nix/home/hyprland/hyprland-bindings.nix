{ config, ... }:
{
  
  # hyprland binds config
  wayland.windowManager.hyprland.settings = {

    # binds
    binds = {
      workspace_back_and_forth = true;
    };

    # bindings
    "$mainMod" = "SUPER";

    # primary key bindings
    bind = [
       
      "$mainMod,  Return, exec, alacritty"
      "$mainMod,  B,      exec, blueman-manager"
      "$mainMod,  C,      exec, alacritty --class clipse --command clipse $PPID"
      "$mainMod,  D,      exec, ~/.dotfiles/apps/wofi/wofi.sh"
      "$mainMod,  E,      exec, nautilus --new-window"
      "$mainMod,  J,      togglesplit"
      "$mainMod,  K,      exec, alacritty --class scratchbox"
      "$mainMod,  L,      exec, hyprlock"
      "$mainMod,  M,      exit,"
      "$mainMod,  N,      exec, alacritty --class nmtui --command ~/.dotfiles/apps/hyprland/nmtui.sh"
      "$mainMod,  P,      exec, ~/.dotfiles/apps/hyprland/hypridle.sh toggle"
      "$mainMod,  Q,      killactive"
      "$mainMod,  S,      exec, XDG_CURRENT_DESKTOP=sway flameshot gui --clipboard --path /home/simon/Pictures/Screenshots"
      "$mainMod,  T,      exec, alacritty --class nostt --command nostt"
      "$mainMod,  V,      togglefloating"

      # alternative key bindings
      "ALT, F, fullscreen"
      "ALT, P, pseudo"
      "ALT, V, togglefloating"

      # focus bindings
      "$mainMod, up,    movefocus, u"
      "$mainMod, down,  movefocus, d"
      "$mainMod, left,  movefocus, l"
      "$mainMod, right, movefocus, r"
    
      # switch workspace bindings
      "$mainMod, 1, workspace, 1"
      "$mainMod, 2, workspace, 2"
      "$mainMod, 3, workspace, 3"
      "$mainMod, 4, workspace, 4"
      "$mainMod, 5, workspace, 5"
      "$mainMod, 6, workspace, 6"
      "$mainMod, 7, workspace, 7"
      "$mainMod, 8, workspace, 8"
      "$mainMod, 9, workspace, 9"
      "$mainMod, 0, workspace, 10"

      # move window to workspaces bindings
      "$mainMod SHIFT, 1, movetoworkspace, 1"
      "$mainMod SHIFT, 2, movetoworkspace, 2"
      "$mainMod SHIFT, 3, movetoworkspace, 3"
      "$mainMod SHIFT, 4, movetoworkspace, 4"
      "$mainMod SHIFT, 5, movetoworkspace, 5"
      "$mainMod SHIFT, 6, movetoworkspace, 6"
      "$mainMod SHIFT, 7, movetoworkspace, 7"
      "$mainMod SHIFT, 8, movetoworkspace, 8"
      "$mainMod SHIFT, 9, movetoworkspace, 9"
      "$mainMod SHIFT, 0, movetoworkspace, 10"

      # move workspaces using the mouse
      "$mainMod, mouse_up,   workspace, e+1"
      "$mainMod, mouse_down, workspace, e-1"

      # move window using keys
      "ALT SHIFT, up,    movewindow, u"
      "ALT SHIFT, down,  movewindow, d"
      "ALT SHIFT, left,  movewindow, l"
      "ALT SHIFT, right, movewindow, r"

      # resize window bindings
      "$mainMod SHIFT, up,    resizeactive, 0 -50"
      "$mainMod SHIFT, down,  resizeactive, 0 50"
      "$mainMod SHIFT, left,  resizeactive, -50 0"
      "$mainMod SHIFT, right, resizeactive, 50 0"

      # multimedia key bindings
      ",XF86AudioMute,         exec, wpctl set-mute @DEFAULT_SINK@ toggle"
      ",XF86AudioRaiseVolume,  exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
      ",XF86AudioLowerVolume,  exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
      ",XF86MonBrightnessDown, exec, brightnessctl set 10%-"
      ",XF86MonBrightnessUp,   exec, brightnessctl set 10%+"

    ];

    # bindl section
    bindl = [

      # manage lockscreen
      ",switch:on:Lid Switch,  exec, hyprlock"

    ];

    # bindm section
    bindm = [

      # move windows using the mouse
      "$mainMod, mouse:272, movewindow"
      "$mainMod, mouse:273, resizewindow"

    ];
  };
}
