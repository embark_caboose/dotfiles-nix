{ config, ... }:
{
  
  # hyprland general config
  wayland.windowManager.hyprland.settings = {

    # gesture secion
    gestures = {
      workspace_swipe                    = true;
      workspace_swipe_min_speed_to_force = "80";
      workspace_swipe_cancel_ratio       = "0.2";
    };

    # input section
    input = {
      kb_layout    = "us";
      follow_mouse = "1";
      sensitivity  = "0";

      touchpad = {
        disable_while_typing = true;
        drag_lock            = true;
        natural_scroll       = true;
        tap-and-drag         = true;
      };
    };
  };
}
