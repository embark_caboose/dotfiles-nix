{ config, ... }:
{
  
  # hyprland general config
  wayland.windowManager.hyprland.settings = {

    # dwindle section
    dwindle = {
      pseudotile           = true;
      preserve_split       = true;
      special_scale_factor = "0.8";
    };

    # general section
    general = {
      border_size = "3";

      "col.active_border"   = "rgba(528bffaa) rgba(043a8baa) 45deg";
      "col.inactive_border" = "rgba(595959aa)";

      gaps_in  = "5";
      gaps_out = "20";

      layout = "dwindle";
    };

    # misc section
    misc = {
      disable_hyprland_logo    = true;
      disable_splash_rendering = true;
    };
  };
}
