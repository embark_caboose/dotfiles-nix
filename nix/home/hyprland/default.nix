{config, pkgs, ... }: 
{
  # install hyprland
  wayland.windowManager.hyprland = {
    enable          = true;
    package         = pkgs.unstable.hyprland;
    xwayland.enable = true;
    systemd.enable  = true;
  };

  imports = [ 
    ./hyprland-bindings.nix
    ./hyprland-environment.nix
    ./hyprland-execution.nix
    ./hyprland-general.nix
    ./hyprland-graphics.nix
    ./hyprland-input.nix
    ./hyprland-window_rules.nix
    ./hyprlock.nix
  ]; 
}
