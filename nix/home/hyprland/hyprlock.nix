{ config, ... }:
{
  
  # include hyprland configs
  home.file.".config/hypr/hyprlock.conf".text = ''

    # general
    general {
      no_fade_in  = false
      no_fade_out = false
    }


    # background
    background {
      monitor =
      path    = $HOME/Pictures/wallpaper.png
    }


    # password input
    input-field {
      monitor =
      
      dots_center  = true
      dots_size    = 0.2
      dots_spacing = 0.64

      fade_on_empty    = true
      hide_input       = false
      placeholder_text = <b> 1 2 3 4 5 ... </b>
      
      position = 0, 150
      size     = 250, 50

      outline_thickness = 3
      font_color        = rgb(fab387)
      inner_color       = rgb(131517)
      outer_color       = rgb(528bff)

      halign = center
      valign = bottom
    }


    # time
    label {
      monitor =

      text     = cmd[update:1000] echo "<b><big> $(date +"%H:%M:%S") </big></b>"
      position = 0, 20

      color       = rgb(fab387)
      font_size   = 64
      font_family = JetBrainsMonoNL NF

      halign = center
      valign = center
    }


    # date and time
    label {
      monitor =

      text     = cmd[update:1000] echo "$(date +%A) $(date +%d) $(date +%B)"
      position = 0, 0

      color       = rgb(fab387)
      font_size   = 20
      font_family = JetBrainsMonoNL NF

      halign = center
      valign = center
    }


    # unlock
    label {
      monitor =

      text     = 👤 siempie
      position = 0, 120

      color       = rgb(528bff)
      font_size   = 14
      font_family = JetBrainsMonoNL NF

      halign = center
      valign = bottom
    }
  '';
}
