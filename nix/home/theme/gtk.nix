{ config, pkgs, ... }:
{
  # default cursor
  home.pointerCursor = {
    name    = "Bibata-Modern-Ice";
    size    = 24;
    package = pkgs.bibata-cursors;
  };

  # dconf settings
  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
      gtk-theme    = "Catppuccin-Macchiato-Standard-Sky-Dark";
      cursor-theme = config.home.pointerCursor.name;
      cursor-size  = config.home.pointerCursor.size;
    };
  };

  # gtk  
  gtk = {
    enable = true;

    # theme
    theme = {
      name    = "Catppuccin-Macchiato-Standard-Sky-Dark";
      package = pkgs.catppuccin-gtk.override {
        accents = [ "sky" ];
        size    = "standard";
        variant = "macchiato";
      };
    };

    # cursor
    cursorTheme = {
      name    = config.home.pointerCursor.name;
      size    = config.home.pointerCursor.size;
      package = config.home.pointerCursor.package;
    };
  };

  # xdg settings
  xdg.configFile = {
    "gtk-4.0/assets".source       = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/assets";
    "gtk-4.0/gtk.css".source      = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk.css";
    "gtk-4.0/gtk-dark.css".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk-dark.css";
  };
}
