{ config, pkgs, ... }:
{
  
  # alacritty
  programs.alacritty = {
    enable   = true;
    package  = pkgs.alacritty;
    settings = {

      # general
      live_config_reload     = true;
      mouse.hide_when_typing = true;
      scrolling.history      = 20000;
      shell.program          = "zsh";

      # environment
      env = {
        "TERM" = "xterm-256color";
      };

      # colors
      colors = {
        
        # bright colors
        bright = {
          black   = "#545454";
          blue    = "#729fcf";
          cyan    = "#34e2e2";
          green   = "#8ae234";
          magenta = "#ad7fa8";
          red     = "#ef2929";
          white   = "#eeeeec";
          yellow  = "#fce94f";
        };

        # normal colors
        normal = {
          black   = "#000000";
          blue    = "#3465a4";
          cyan    = "#06989a";
          green   = "#4e9a06";
          magenta = "#75507b";
          red     = "#cc0000";
          white   = "#d3d7cf";
          yellow  = "#c4a000";
        };

        # primary colors
        primary = {
          background = "#000000";
          foreground = "#ffffff";
        };
      };

      # cursor
      cursor = {
        blink_interval = 750;
        style = {
          blinking = "always";
          shape    = "underline";
        };
      };

      # font
      font = {
        size = 14.0;

        # normal font
        normal = {
          family = "JetBrainsMonoNL NF";
          style  = "Light";
        };

        # bold font
        bold = {
          family = "JetBrainsMonoNL NF";
          style  = "Light";
        };

        # italic font
        italic = {
          family = "JetBrainsMonoNL NF";
          style  = "Light";
        };
      };

      # keyboard
      keyboard.bindings = [{
          action = "SpawnNewInstance";
          key    = "Return";
          mods   = "Control|Shift";
        }
      ];

      # window
      window = {
        opacity = 0.75;
        class   = {
          general  = "Alacritty";
          instance = "Alacritty";
        };
        padding = {
          x = 7;
          y = 5;
        };
      };
    };
  };
}
