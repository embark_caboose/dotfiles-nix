{ config, pkgs, ... }:
{
  
  # git
  programs.git = {
    enable    = true;
    userName  = "Simon Cornet";
    userEmail = "simon@simoncor.net";
    aliases   = {
    
      # commit + message
      cam = "commit -am";

    };
    extraConfig = {

      # generic
      advice.addIgnoredFile = false;
      init.defaultBranch    = "master";
      push.default          = "matching";
      web.browser           = "firefox";

      # core config
      core = {
        editor   = "vim";
        filemode = true;

        packedGitLimit      = "512m";
        packedGitWindowSize = "512m";
        compression         = 0;
      };

      # credential cache
      credential = {
        helper = "cache --timeout 172800";
      };
    };
  };
}
