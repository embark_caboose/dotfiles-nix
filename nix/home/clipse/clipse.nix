{ config, pkgs, ... }:
{
  
  # configure clipse
  home.file.".config/clipse/config.json".text = ''
    {
      "historyFile": "clipboard_history.json",
      "maxHistory": 50,
      "themeFile": "custom_theme.json",
      "tempDir": "tmp_files"
    }
  '';

  # configure clipse theme
  home.file.".config/clipse/custom_theme.json".text = ''
    {
      "useCustomTheme": true,
      "DimmedDesc": "#ffffff",
      "DimmedTitle": "#ffffff",
      "FilteredMatch": "#528bff",
      "NormalDesc": "#ffffff",
      "NormalTitle": "#ffffff",
      "SelectedDesc": "#3465a4",
      "SelectedTitle": "#528bff",
      "SelectedBorder": "#528bff",
      "SelectedDescBorder": "#528bff",
      "TitleFore": "#ffffff",
      "Titleback": "#528bff",
      "StatusMsg": "#ffffff"
    }
  '';
}
