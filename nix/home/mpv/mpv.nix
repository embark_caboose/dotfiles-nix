{ config, ... }:
{
  
  # enable mpv
  programs.mpv = {
    enable = true;
    config = {
      hwdec   = "auto";
      profile = "gpu-hq";
      vo      = "gpu";
    };
  };
}
