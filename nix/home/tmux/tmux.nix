{ config, pkgs, ... }:
{
  
  # tmux
  programs.tmux = {
    enable  = true;

    # settings
    clock24      = true;
    historyLimit = 20000;
    keyMode      = "vi";
    mouse        = true;

    # extra config
    extraConfig = ''
      # override colors
      set-option -sa terminal-overrides ",xterm*:Tc"

      # set screen
      set-option -g default-terminal "screen-256color"

      # escape time
      set-option -sg escape-time 10

      # start windows and panes at 1, not 0
      set -g base-index 1
      set -g pane-base-index 1
      set-window-option -g pane-base-index 1
      set-option -g renumber-windows on

      # use alt-arrow keys without prefix key to switch panes
      bind -n M-Left select-pane -L
      bind -n M-Right select-pane -R
      bind -n M-Up select-pane -U
      bind -n M-Down select-pane -D

      # shift arrow to switch windows
      bind -n S-Left  previous-window
      bind -n S-Right next-window

      # keybindings
      bind-key -T copy-mode-vi v send-keys -X begin-selection
      bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
      bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

      # split windows
      bind '"' split-window -v -c "#{pane_current_path}"
      bind % split-window -h -c "#{pane_current_path}"
    '';

    # configure plugins
    plugins = with pkgs; [ {

        # resurrect
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";

      } {

        # continuum
        plugin = tmuxPlugins.continuum;
        extraConfig = ''
          set -g @continuum-restore 'on'
          set -g @continuum-save-interval '60'
        '';

      } {

        # vim-tmux-navigator
        plugin = tmuxPlugins.vim-tmux-navigator; 

      } {

        # catppuccin theme
        plugin      = tmuxPlugins.catppuccin;
        extraConfig = ''
          set -g @catppuccin_flavour 'mocha'
        '';
      } {

        # yank
        plugin = tmuxPlugins.yank; 

      }
    ];
  };
}
