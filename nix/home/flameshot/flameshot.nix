{ config, ... }:
{

  # include flameshot configs
  home.file.".config/flameshot/flameshot.ini".text = ''

    [General]
    contrastOpacity=188
    contrastUiColor=#528bff
    checkForUpdates=false
    drawFontSize=8
    drawThickness=3
    uiColor=#043a8b
    savePath=/home/simon/Pictures/Screenshots/
    showHelp=false
    showStartupLaunchMessage=false

  '';
}
