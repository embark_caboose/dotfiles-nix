{ config, pkgs, ... }:
{

  # ssh
  programs.ssh = {
    enable      = true;
    matchBlocks = {

      # general
      "*" = {
        user                  = "simon";
        identityFile          = "${config.home.homeDirectory}/.ssh/talathiel";
        serverAliveInterval   = 60;
      };

      # siempie lab
      "*.do.local"            = { proxyJump = "bastion.siempie.com"; };
      "*.hackerboys.internal" = { proxyJump = "bastion.siempie.com"; };
      "*.siempie.internal"    = { proxyJump = "bastion.siempie.com"; };
      "*.siempie.local"       = { proxyJump = "bastion.siempie.com"; };
    };
  };
}
