{ config, pkgs, ... }:
{

  # zoxide
  programs.zoxide = {
    enable               = true;
    enableZshIntegration = true;
    options              = [ "--cmd cd" ];
  };
  
  # zsh
  programs.zsh = {

    # general
    enable = true;

    # settings
    enableCompletion          = true;
    autosuggestion.enable     = true;
    syntaxHighlighting.enable = true;

    # oh-my-zsh
    oh-my-zsh = {
      enable  = true;
      plugins = [ "git" ];
      theme   = "bira";
    };

    # aliasses
    shellAliases = {

      # logout from hyprland
      tata = "hyprctl dispatch exit";

      # git
      gfo = "git fetch origin";
      gmo = "git merge origin";
      gpo = "git push origin";
      gfu = "git fetch upstream";
      gmu = "git merge upstream";

      # k9s
      k9s-hyperspace = "ssh k9s.siempie.internal -t /home/simon/.local/bin/k9s --context hyperspace";
      k9s-kaas       = "ssh k9s.siempie.internal -t /home/simon/.local/bin/k9s --context admin@kaas";
      k9s-siempie    = "ssh k9s.siempie.internal -t /home/simon/.local/bin/k9s --context siempie";

      # nixos-rebuild
      system-rebuild    = "sudo nixos-rebuild switch --flake ~/.dotfiles#`hostname -s`";
      system-update     = "sudo nix flake update nixpkgs -I ~/.dotfiles && sudo nixos-rebuild switch --upgrade --flake ~/.dotfiles#`hostname -s` && sudo fwupdmgr upgrade";
      system-clean      = "sudo nix-store --gc";
      system-purge      = "sudo nix-collect-garbage -d";
      system-full-clean = "sudo nix-store --gc && sudo nix-collect-garbage -d";

      # sshfs
      sj-mount-media   = "sshfs nas.siempie.local:/tank/media /home/simon/remotedir/media";
      sj-mount-siempie = "sshfs nas.siempie.local:/tank/siempie /home/simon/remotedir/siempie";

      umount-media     = "sudo umount /home/simon/remotedir/media";
      umount-siempie   = "sudo umount /home/simon/remotedir/siempie";

      # teleport
      titc = "tsh login --proxy=teleport.itcreation.nl --user=admscornet";
    };
  };
}

