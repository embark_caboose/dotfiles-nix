{ config, pkgs, ... }:
{
  
  # mako
  services = {
    mako = {
      enable = true;

      # sorting
      sort = "-time";
      
      # location
      anchor  = "top-right";
      layer   = "top";
      padding = "20,20,10";
      margin  = "10,10,10";

      # timeout
      defaultTimeout = 6000;
      ignoreTimeout  = true;

      # theme
      font = "FiraCode Nerd Font 12";

      # size
      width  = 350;
      height = 125;
      
      # colors
      backgroundColor = "#131517";
      textColor       = "#a0a4ab";
      borderColor     = "#528bff";
      borderSize      = 2;
      borderRadius    = 8;
    };
  };
}

