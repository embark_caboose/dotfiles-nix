{ config, pkgs, ... }:
{

  # neovim
  programs.neovim = {
    enable        = true;
    defaultEditor = true;
    viAlias       = true;
    vimAlias      = true;

    # neovim config
    extraConfig = ''

      " Use tab for trigger completion with characters ahead and navigate.
      inoremap <silent><expr> <TAB>
        \ pumvisible() ? "\<C-n>" :
        \ <SID>check_back_space() ? "\<TAB>" :
        \ coc#refresh()
      inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
      function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~# '\s'
      endfunction

      " Use <c-space> to trigger completion.
      if has('nvim')
        inoremap <silent><expr> <c-space> coc#refresh()
      else
        inoremap <silent><expr> <c-@> coc#refresh()
      endif

      " clipboard
      set clipboard+=unnamedplus

      " terminal title
      set title

      " auto-complete
      set completeopt=menu,menuone,longest

      " show number
      set number

      " set no wrap
      set nowrap
    '';

    # lua config
    extraLuaConfig = ''

      -- set mapleader
      vim.g.mapleader = ' '
      vim.g.maplocalleader = ' '

      -- set vim options
      vim.opt.backspace  = '2'
      vim.opt.laststatus = 2
      vim.opt.cursorline = true
      vim.opt.guicursor= { 'a:hor20-Cursor/lCursor' }

      -- set scrolloff
      vim.opt.scrolloff = 10

      -- indentation
      vim.opt.tabstop     = 2
      vim.opt.shiftwidth  = 2
      vim.opt.expandtab   = true
      vim.opt.smartindent = true

      -- vim leave function
      vim.api.nvim_create_autocmd({ "VimLeave" }, {
        callback = function()
          vim.cmd('set guicursor=a:hor20-Cursor/lCursor')
        end,
      })


      -- load plugins
      require("gitsigns").setup()
      vim.keymap.set("n", "<leader>gp", ":Gitsigns preview_hunk<CR>", {})
      vim.keymap.set("n", "<leader>gb", ":Gitsigns toggle_current_line_blame<CR>", {})

      -- neo-tree
      require("neo-tree").setup({
        filesystem = {
          filtered_items = {
            visible = true,
            hide_dotfiles = false,
            hide_gitignored = false,
          },
        },
        window = {
          position = "left",
          width = 45,
          mapping_options = {
            noremap = true,
            nowait = true,
          },
          mappings = {
            ["<space>"] = {
              "toggle_node",
              nowait = false,
            },
          },
        },
      })
      require("neo-tree.sources.manager").show("filesystem")


      -- nvim-treesitter
      require("nvim-treesitter").setup({
        build = ":TSUpdate",
        config = function ()
          local configs = require("nvim-treesitter.configs")
          configs.setup({
            ensure_installed = { "ansible", "bash", "comment", "nix,", "python", "yaml" },
            sync_install = false,
            highlight = { enable = true },
            indent = { enable = true },
          })
        end
      })


      -- telescope-nvim
      local builtin = require("telescope.builtin")
      vim.keymap.set('n', '<C-p>', builtin.find_files, {})


      -- vimplugin-catppuccin-nvim
      require("catppuccin").setup({ 
        flavour = "mocha",
        transparent_background = "true"
      })
      vim.cmd.colorscheme "catppuccin"
    '';

    # auto-complete
    coc = {
      enable   = true;
      settings = {
        "suggest.noselect"        = true;
        "suggest.enablePreview"   = true;
        "suggest.enablePreselect" = true;
        "suggest.disableKind"     = true;

        "diagnostic.errorSign" = "✘";
        "diagnostic.infoSign"  = "ℹ";
        "diagnostic.hintSign"  = "➤";
      };
    };

    # plugins
    plugins = with pkgs.vimPlugins; [
      gitsigns-nvim
      neo-tree-nvim
      nvim-treesitter
      telescope-nvim
      catppuccin-nvim
    ];
  };
}

