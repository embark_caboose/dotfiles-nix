#!/usr/bin/env zsh

# check if wofi is already running
pkill wofi

# start wofi
wofi \
  --show drun \
  --no-action \
  --width=600 \
  --height=600 \
  --normal-window \
  --allow-images \
  --prompt="Applications ..." \
  --style=/home/simon/.dotfiles/apps/wofi/style.css
