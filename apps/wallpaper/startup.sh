#!/usr/bin/env zsh

# check if swaybg is already running
pkill swaybg

# config
wallpaper=(~/Pictures/wallpaper.jpg)
wallpaper_url='https://wallscloud.net/img/resize/3840/2160/MM/2022-03-30-blurred.jpg'

# download the wallpaper
if [ ! -f ${wallpaper} ]; then
  wget -q -O ${wallpaper} ${wallpaper_url}
fi

# start swaybg
swaybg -m fill -i ${wallpaper} &
