{

  // generic
  "exclusive": true,
  "gtk-layer-shell": true,
  "height": 0,
  "layer": "top",
  "mod": "dock",
  "passthrough": false,
  "position": "top",

  // modules location
  "modules-left": [
    "hyprland/workspaces",
    "hyprland/window",
  ],

  "modules-center": [
    "clock"
  ],

  "modules-right": [
    "tray",
    "cpu",
    "memory",
    "disk",
    "group/connections",
    "pulseaudio",
    "backlight",
    "battery",
    "custom/hypridle",
  ],

  // modules
  "backlight": {
    "device": "intel_backlight",
    "format": "{icon} {percent}%",
    "format-icons": [
      "󰃞",
      "󰃟",
      "󰃠"
    ],
    "on-scroll-up": "brightnessctl set 5%+",
    "on-scroll-down": "brightnessctl set 5%-"
  },

  "battery": {
		"format": "{icon} ",
    "format-full": "{icon} ",
		"format-icons": [
			"",
			"",
      "",
      "",
      ""
		],
	},

  "clock": {
    "format": "{:%R    %d-%m}",
    "timezones": ["Europe/Amsterdam", "Asia/Kolkata"],
    "actions": { "on-click": "tz_down" },
    "tooltip-format": "<tt><small>{calendar}</small></tt>"
  },

  "cpu": {
		"interval": 2,
		"format": " {usage:2}%"
	},

  "custom/hypridle": {
    "interval": 1,
    "format": "{} ",
    "exec": "$HOME/.dotfiles/apps/hyprland/hypridle.sh",
    "on-click": "$HOME/.dotfiles/apps/hyprland/hypridle.sh toggle"
  },

  "custom/wireguard": {
    "interval": 1,
    "format": "{}",
    "exec": "$HOME/.dotfiles/apps/wireguard/wireguard.sh status",
    "on-click": "$HOME/.dotfiles/apps/wireguard/wireguard.sh switch"
  },

  "disk": {
    "interval": 10,
    "path": "/",
    "format": "󱂵 {percentage_used}%",
  },

  "hyprland/window": {
    "format": "{}"
  },

  "hyprland/workspaces": {
    "all-outputs": true,
    "sort-by-number": true,
    "format": "{icon}",
    "format-icons": {
      "1": "1",
      "2": "2",
      "3": "3",
      "4": "4",
      "5": "5",
      "6": "6",
      "7": "7",
      "8": "8",
      "9": "9",
      "10": "10"
    }
  },

  "memory": {
		"interval": 2,
		"format": " {}%",
    "tooltip-format": "{used:0.1f}GB / {total:0.1f}GB",
	},

  "network": {
    "tooltip": false,
    "format-wifi": "󰣶 {signalStrength}%",
    "format-ethernet": "󰱓 ",
    "on-click": "alacritty --class nmtui --command ~/.dotfiles/apps/hyprland/nmtui.sh"
  },

  "network#full": {
    "tooltip": true,
    "format-wifi": "{essid}",
    "format-ethernet": "{ipaddr}/{cidr}",
    "tooltip-format-wifi": "SSID: {essid}\nSignal Strength: {signalStrength}%\nIP: {ipaddr}\nNetmask: {netmask} (/{cidr})\nGateway: {gwaddr}",
    "tooltip-format-ethernet": "IP: {ipaddr}\nNetmask: {netmask} (/{cidr})\nGateway: {gwaddr}",
    "on-click": "alacritty --class nmtui --command ~/.dotfiles/apps/hyprland/nmtui.sh"
  },

  "pulseaudio": {
		"format": "{icon} {volume:3}%",
		"format-muted": "󰖁 ",
		"format-icons": {
      "headphone": "",
      "hands-free": "",
      "headset": "",
      "phone": "",
      "portable": "",
      "car": "",
      "default": ["", "", ""]
    },
		"scroll-step": 5,
		"on-click": "pavucontrol"
	},

  "tray": {
    "icon-size": 13,
    "spacing": 10
  },

  "group/connections": {
    "orientation": "horizontal",
      "drawer": {
        "transition-duration": 500,
        "children-class": "network",
        "transition-left-to-right": true
      },
    "modules": [
      "network",
      "custom/wireguard",
      "network#full",
    ]
  },
}
