#!/usr/bin/env zsh

# check if waybar is already running
pkill waybar

# start waybar
waybar \
  -c ~/.dotfiles/apps/waybar/waybar.conf \
  -s ~/.dotfiles/apps/waybar/style.css \
  &
