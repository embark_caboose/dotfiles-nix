#!/usr/bin/env zsh

# sleep
sleep 2s

# gather monitor count
monitor_count=$(hyprctl monitors -j | jq '. | length')

# single monitor
if [[ "$monitor_count" == 1 ]]; then

  # set primary monitor
  primary_monitor=$(hyprctl monitors -j | jq -r '.[0].name')

  # move all workspaces to primary monitor
  for ((ws = 1; ws <= 10; ws++)); do
    hyprctl keyword wsbind $ws,$primary_monitor > /dev/null
    hyprctl dispatch moveworkspacetomonitor $ws $primary_monitor > /dev/null
  done


# dual monitor
elif [[ "$monitor_count" == 2 ]]; then

  # set both monitors
  primary_monitor=$(hyprctl monitors -j | jq -r '.[0].name')
  external_monitor=$(hyprctl monitors -j | jq -r '.[1].name')

  # move workspace 1 to primary monitor
  hyprctl keyword wsbind 1,$primary_monitor > /dev/null
  hyprctl dispatch moveworkspacetomonitor 1 $primary_monitor > /dev/null

  # move workspace 2-8 to
  for ((ws = 2; ws <= 8; ws++)); do
    hyprctl keyword wsbind $ws,$external_monitor > /dev/null
    hyprctl dispatch moveworkspacetomonitor $ws $external_monitor > /dev/null
  done

else  # more than 2 monitors...
  echo "Not supported"
fi
