#!/usr/bin/env zsh

# sleep some time
sleep 2s

# check if nextcloud is already running
pkill nextcloud

# start nextcloud
nextcloud
