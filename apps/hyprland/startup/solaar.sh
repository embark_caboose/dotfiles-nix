#!/usr/bin/env zsh

# sleep some time
sleep 2s

# check if solaar is already running
pkill solaar

# start solaar
solaar --window=hide
