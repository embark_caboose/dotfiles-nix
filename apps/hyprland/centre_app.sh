#!/usr/bin/env bash

# manage arguments 
while [[ $# -gt 0 ]]; do
  case "$1" in
    --class)
      app_class="$2"
      shift 2
      ;;
    --title)
      app_title="$2"
      shift 2
      ;;
    *)
      echo "Invalid option: $1"
      exit 1
      ;;
  esac
done


# function to center the app
handle() {
  event="${1%%>>*}"
  edata="${1#*>>}"
  IFS=',' read -ra array <<< "$edata"

  if [[ $event = "openwindow" && ${array[2]} = "${app_class}" && ${array[3]} = "${app_title}" ]]; then
    hyprctl dispatch centerwindow
  fi
}

# read the socket and activate the function
while IFS= read -r line; do
  handle "$line"
done < <(socat -U - UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock)