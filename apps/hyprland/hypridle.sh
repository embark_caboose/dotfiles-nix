#!/usr/bin/env zsh


# function to toggle hypridle
function toggle () {
  if pgrep "hypridle" > /dev/null
	then
		pkill hypridle
		notify-send -u normal -t 4500 "󰐩  Hypridle Inactive"
	else
		hypridle &
		notify-send -u normal -t 4500 "󰐨  Hypridle Active"
	fi
}

# main function
case $1 in
  toggle)
		toggle
		;;
	*)
		if pgrep "hypridle" > /dev/null
		then
			icon="󰐨"
		else
			icon="󰐩"
		fi
		printf "%s" "$icon"
		;;
esac
