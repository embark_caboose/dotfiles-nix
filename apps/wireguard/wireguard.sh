#!/usr/bin/env zsh

# variables
hostname_short=$(hostname --short)
tailscale_bin="tailscale"
tailscale_status=$("$tailscale_bin" status)

# status vpn
status_vpn () {
  vpn_status=""

  # check for tailscale status
  if echo "$tailscale_status" | grep "$hostname_short" &> /dev/null; then
    vpn_status+=' '
  else
    vpn_status+=' '
  fi

  # check for direct connectivity
  if "$tailscale_bin" status | grep allegro | grep direct &> /dev/null; then
    vpn_status+='󰞁'
  else
    vpn_status+='󱀀'
  fi

  echo "$vpn_status"
}

# switch vpn
switch_vpn () {
  if echo "$tailscale_status" | grep "$hostname_short" &> /dev/null; then
    "$tailscale_bin" down
  else
    "$tailscale_bin" up
  fi
}

# main function
case $1 in
  switch)
    switch_vpn &> /dev/null
    ;;
  status)
    status_vpn
    ;;
  *)
    echo "Usage: $0 {switch|status}"
    exit 1
    ;;
esac
