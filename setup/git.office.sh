#!/usr/bin/env zsh

# config
git_host="gitlab.itcreation.nl"
git_token=""
git_user="admscornet"
git_repo_dir=~/Documents/git/"IT creation"
git_repos=(
  "itc-ansible"
)

# get the git token
vared -p "Enter your git token: " -c git_token
echo ""

# create folder if it doesn't exist
mkdir -p "${git_repo_dir}"

# clone repositories
for git_repo in "${git_repos[@]}"; do

  # check if repo exists
  if [ -f ${git_repo_dir}/${git_repo} ]; then
    echo "🚫 error: directory exists"
    exit
  fi

    # clone repository
    git clone https://${git_user}:${git_token}@${git_host}/${git_user}/${git_repo} ${git_repo_dir}/${git_repo} &> /dev/null

    # print success message
    echo "✅ done: ${git_host}/${git_user}/${git_repo}"

done
