# ❄️ NixOS configuration

[Install](https://git.simoncor.net/siempie/dotfiles-nix/src/branch/main/docs/install)  
[Upkeep](https://git.simoncor.net/siempie/dotfiles-nix/src/branch/main/docs/upkeep)  
[Usage](https://git.simoncor.net/siempie/dotfiles-nix/src/branch/main/docs/usage)  

  preview-1 | preview-2
| --------- | --------- |
![](https://git.simoncor.net/siempie/dotfiles-nix/raw/branch/main/docs/images/preview-1.png) | ![](https://git.simoncor.net/siempie/dotfiles-nix/raw/branch/main/docs/images/preview-2.png) 

### Applications
|                          |                                        |
| ------------------------ | -------------------------------------- |
| **Application Launcher** | Wofi                                   |
| **Bar**                  | Waybar                                 |
| **Browsers**             | Firefox, Google Chrome, Microsoft Edge |
| **Clipboard Manager**    | clipse                                 |
| **Desktop Environment**  | Gnome, Hyprland (primary)              |
| **Display Server**       | Wayland                                |
| **File Manager**         | Nautilus                               |
| **Image Viewer**         | Loupe                                  |
| **Image Editor**         | GIMP                                   |
| **Music Player**         | Spotify                                |
| **Shell**                | zsh                                    |
| **Screenshots**          | grimblast, satty                       |
| **Password Manager**     | Bitwarden                              |
| **Terminal Emulator**    | Alacritty                              |
| **Text Editors**         | (neo)vim, Visual Studio Code           |
| **Video Player**         | mpv                                    |
| **Virtualisation**       | virt-manager                           |
| **VPN Client**           | Tailscale                              |

#### Enjoy!
